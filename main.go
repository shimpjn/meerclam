package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"encoding/json"
	"io"
	"log"
	"net"
	"path/filepath"
	"strings"
	"github.com/go-redis/redis/v8"
)

type Config struct {
	RedisDB   *redis.Client
	ClamAddr  string
	Subscribe string
	Publish   string
	Filestore string
}

type StoredFile struct {
	Name     string `json:"name"`
	Location string `json:"location"`
	Status   string `json:"status"`
}

func main() {
	var (
		config   Config
		redisURL string
	)

	flag.StringVar(&redisURL, "redis", "redis://localhost:6379", "Redis URL")
	flag.StringVar(&config.ClamAddr, "clamd", "localhost:3310", "Clamd socket")
	flag.StringVar(&config.Subscribe, "subscribe", "suricata", "Redis channel of Suricata events")
	flag.StringVar(&config.Publish, "publish", "clamd", "Redis channel to publish events to")
	flag.StringVar(&config.Filestore, "filestore", "/mnt/filestore", "Root directory of Suricata filestore")

	flag.Parse()

	opt, err := redis.ParseURL(redisURL)
	if err != nil {
		log.Fatal(err)
	}

	config.RedisDB = redis.NewClient(opt)
	ctx := context.TODO()
	if err := listenForFileInfo(ctx, config); err != nil {
		log.Print(err)
	}
}

func listenForFileInfo(ctx context.Context, config Config) error {
	pubsub := config.RedisDB.Subscribe(ctx, config.Subscribe)

	for {
		msg, err := pubsub.ReceiveMessage(ctx)
		if err != nil {
			return err
		}

		var eveRecord map[string]interface{}
		if err := json.Unmarshal([]byte(msg.Payload), &eveRecord); err != nil {
			return err
		}

		eventType, ok := eveRecord["event_type"]
		if !ok {
			continue
		}

		if eventType != "fileinfo" {
			continue
		}

		object, ok := eveRecord["fileinfo"]
		if !ok {
			continue
		}

		fileinfo, ok := object.(map[string]interface{})
		if !ok {
			continue
		}

		filename, ok := fileinfo["filename"].(string)
		if !ok {
			continue
		}

		sha256, ok := fileinfo["sha256"].(string)
		if !ok {
			continue
		}

		if len(sha256) != 64 {
			continue
		}

		storedFile := StoredFile{
			Name: filepath.Base(filename),
			Location: filepath.Join(config.Filestore, sha256[:2], sha256),
			Status: "NONE",
		}

		go clamScan(ctx, config.RedisDB, config.ClamAddr, config.Publish, storedFile)
	}
}

func clamScan(ctx context.Context, rdb *redis.Client, clamAddr, publish string, file StoredFile) {
	conn, err := net.Dial("tcp", clamAddr)
	if err != nil {
		log.Print(err)
		return
	}
	defer conn.Close()

	if _, err = fmt.Fprintf(conn, "SCAN %s\n", file.Location); err != nil {
		log.Print(err)
		return
	}

	buffer := &bytes.Buffer{}

	if _, err := io.Copy(buffer, conn); err != nil {
		log.Print(err)
		return
	}

	line, err := buffer.ReadString('\n')
	if err != nil {
		log.Print(err)
		return
	}

	parts := strings.Split(line, ": ")
	if len(parts) != 2 {
		log.Printf("Cannot parse clamd response: %s", line)
		return
	}
	
	file.Status = strings.TrimSpace(parts[1])
	message, err := json.Marshal(file)
	if err != nil {
		log.Print(err)
		return
	}

	cmd := rdb.Publish(ctx, publish, message)
	if err := cmd.Err(); err != nil {
		log.Print(err)
		return
	}
}
